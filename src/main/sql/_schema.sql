CREATE SCHEMA otp;

USE otp;

create user 'springuser'@'%' identified by 'springpass';

grant all on otp.* to 'springuser'@'%';