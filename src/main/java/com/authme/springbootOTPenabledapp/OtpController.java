package com.authme.springbootOTPenabledapp;

import com.authme.springbootOTPenabledapp.mail.EmailService;
import com.authme.springbootOTPenabledapp.mail.EmailTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.constraints.Email;
import java.util.HashMap;
import java.util.Map;

@Controller
@Validated
public class OtpController {

    @Autowired
    OtpService otpService;

    @Autowired
    EmailService emailService;

    @GetMapping("/generateOtp")
    @ResponseStatus( HttpStatus.OK )
    public void generateOTP(@Email @RequestParam("email") String email) throws MessagingException {

            // Get user logged in data
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            String username = auth.getName();
            int otp = otpService.generateOTP(username);

            //Generate The Template to send OTP
            EmailTemplate template = new EmailTemplate("SendOtp.html");
            Map<String,String> replacements = new HashMap<String,String>();
            replacements.put("user", username);
            replacements.put("otpnum", String.valueOf(otp));
            String message = template.getTemplate(replacements);
            emailService.sendOtpMessage(email, "One time password", message);

    }

    @PostMapping(value ="/validateOtp")
    @ResponseBody
    public String validateOtp(@RequestParam("otpnum") int otpnum){

        final String SUCCESS = "Entered Otp is valid";
        final String FAIL = "Entered Otp is NOT valid. Please Retry!";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        //Validate the Otp
        if(otpnum >= 0){

            int serverOtp = otpService.getOtpCode(username);
            if(serverOtp > 0){
                if(otpnum == serverOtp){
                    otpService.clearOTP(username);

                    return SUCCESS;
                }
                else {
                    return SUCCESS;
                }
            }else {
                return FAIL;
            }
        }else {
            return FAIL;
        }
    }

}
