package com.authme.springbootOTPenabledapp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class OtpServiceTest {

    @Autowired
    OtpService otpService;

    @Test
    public void otpServiceContextLoads() throws Exception {
        assertThat(otpService).isNotNull();
    }

    @Test
    public void generateUniq100000keysForUser(){
        Set<Integer> keys = new HashSet<>();
        int uniq = 0;
        for (int i = 1; i < 100000 ; i++) {
            int ikey = otpService.generateOTP("testname");
             uniq = keys.add(ikey)? uniq+1 : uniq ;
        }
        assertThat(uniq == 0 );
    }

    @Test
    public void validateOtpTest(){
        int userCode = otpService.generateOTP("testname");
        int validCode = otpService.getOtpCode("testname");
        assertThat(validCode == userCode);
    }

}