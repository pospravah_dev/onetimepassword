package com.authme.springbootOTPenabledapp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class OtpControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    RestTemplate restTemplate;

    @Test
    void generateOtpTest() {
        assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/generateOtp",
                String.class)).contains("Hello, World");
    }
}