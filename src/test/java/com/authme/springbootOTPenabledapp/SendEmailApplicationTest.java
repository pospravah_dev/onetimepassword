package com.authme.springbootOTPenabledapp;

import com.authme.springbootOTPenabledapp.mail.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.mail.MessagingException;

@SpringBootTest
public class SendEmailApplicationTest {

    @Autowired
    private EmailService emailService;

    @Test
    public void testEmail() throws MessagingException {
        emailService.sendOtpMessage("frank23@example.com", "Test subject", "Test mail");
    }
}