# README #

### What is this repository for? ###

* This demo application to generate and validate One Time Password
* Password generated for current user logged in and valid 3 minutes

### Initial setup ###

* MySQL DB need to be installed
* /sql folder contains initial SQL scriprtds with schemas users etc.
* application.properties require smtp mail properties (spring.mail.username, spring.mail.password)
* thymeleaf used for UI

### Use Case ###

1) Run application
ex. http:/localhost:8080
Expeted: Login screen shown

2) Login by one of existing users 
"user"/"user" or "admin"/"admin"
Expected: Redirected to Main application screen

Background request run for this:
POST http://localhost:8080/login HTTP/1.1
username=admin&password=admin

3) Provide email to send One Time Password
Expected: Redirected to One Time Validation screen

Background request run for this:
> GET http://localhost:8080/generateOtp?email=pospravah@ukr.net


4) Check email account, ex.:

Expected:
--- Forwarded message ---
Subject: One time password
Date: 27 November 2020, 15:49:35

Hi admin

Your Otp Number is 390164

Thanks

5) Enter number from email to validation screen above:

Background request run for this:
POST http://localhost:8080/validateOtp HTTP/1.1
otpnum=390164

Expected answer:
Entered Otp is valid

6) Repeat validation:
Entered Otp is NOT valid. Please Retry!

